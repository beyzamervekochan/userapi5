﻿using Business.Services;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;

namespace UserApi5.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _usersService;

        public UsersController(IUserService usersService) =>
            _usersService = usersService;

        [HttpGet]
        public IActionResult Get() =>
            Ok(_usersService.List());

        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            var user = _usersService.Get(id);

            if (user is null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [HttpPost]
        public IActionResult Post(UserModel newUser)
        {
            _usersService.Create(newUser);

            return Ok(newUser);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, UserModel updatedUser)
        {
            var user = _usersService.Get(id);

            if (user is null)
            {
                return NotFound();
            }

            _usersService.Update(updatedUser);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var user = _usersService.Get(id);

            if (user is null)
            {
                return NotFound();
            }

            _usersService.Delete(id);

            return NoContent();
        }



    }
}
