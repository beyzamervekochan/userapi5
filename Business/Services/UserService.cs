﻿using DataAccess.Repositories;
using Entities.Models;

namespace Business.Services
{
    public class UserService : IUserService
    {
        private IMongoRepository<UserModel> mongoRepository;

        public UserService(IMongoRepository<UserModel> mongoRepository)
        {
            this.mongoRepository = mongoRepository;
        }

        public void Create(UserModel user)
        {

            mongoRepository.InsertOne(user);
        }

        public void Delete(string id)
        {
            mongoRepository.DeleteById(id);
        }

        public UserModel? Get(string id)
        {
            return mongoRepository.FindById(id);
        }

        public List<UserModel> List()
        {
            return mongoRepository.AsQueryable().ToList();
        }

        public void Update(UserModel user)
        {
            mongoRepository.ReplaceOne(user);
        }
    }
}
