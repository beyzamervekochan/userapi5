﻿using Entities.Models;
using MongoDB.Bson;

namespace Business.Services
{
    public interface IUserService
    {
        public void Create(UserModel user);
        public UserModel? Get(string id);
        public List<UserModel> List();
        public void Update(UserModel user);
        public void Delete(string id);
    }
}
